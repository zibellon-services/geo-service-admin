import { Layout, Menu } from "antd";
import { observer } from "mobx-react-lite";
import React from "react";
import { useHistory } from "react-router-dom";
import { ROUTES } from "router";
import styles from "./styles.module.scss";

export const NavBlock = observer(() => {
  const history = useHistory();

  const toPage = (url: string) => {
    history.push(url);
  };

  return (
    <Layout.Sider>
      <Menu theme="light" mode="inline" className={styles.menu}>
        <Menu.Item key="1" onClick={() => toPage(ROUTES.AUTOCOMPLETE.path)}>
          Автодополнение
        </Menu.Item>
        <Menu.Item key="2" onClick={() => toPage(ROUTES.SEARCH.path)}>
          Поиск
        </Menu.Item>
        <Menu.Item key="3" onClick={() => toPage(ROUTES.REVERSE.path)}>
          Адрес по координатам
        </Menu.Item>
        <Menu.Item key="4" onClick={() => toPage(ROUTES.WAY.path)}>
          Постройка маршрута
        </Menu.Item>
        <Menu.Item key="5" onClick={() => toPage(ROUTES.ADDRESS.path)}>
          Адреса
        </Menu.Item>
        <Menu.Item key="6" onClick={() => toPage(ROUTES.COMPONENT_ADDRESS.path)}>
          Компоненты
        </Menu.Item>
        <Menu.Item key="7" onClick={() => toPage(ROUTES.COMPONENT_PLACE.path)}>
          Места
        </Menu.Item>
      </Menu>
    </Layout.Sider>
  );
});

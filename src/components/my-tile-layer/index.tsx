import { TileLayer } from "react-leaflet";
import { Constants } from "utils/constants";

export const MyTileLayer = (props: any) => {
  return (
    <TileLayer
      {...props}
      attribution="&copy;"
      url={Constants.TILE_SERVER_URL}
    />
  );
};

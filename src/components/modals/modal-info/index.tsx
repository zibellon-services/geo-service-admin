import { Modal } from 'antd';

export const modalInfo = (content: string) => {
  Modal.info({
    title: 'Уведомление',
    content,
    onOk() {},
  });
};

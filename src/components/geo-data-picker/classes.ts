export interface MapGeoData {
  point: number[];
  polygon: number[][][];
}

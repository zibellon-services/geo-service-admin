import { Button, Row, Space } from 'antd';
import { MyTileLayer } from 'components/my-tile-layer';
import { getCenter } from 'geolib';
import { LatLngExpression } from 'leaflet';
import { useEffect, useState } from 'react';
import { MapContainer, Marker, Polygon, useMapEvents } from 'react-leaflet';
import { Constants } from 'utils/constants';
import { MapGeoData } from './classes';

type Props = {
  zoom?: number;
  value?: MapGeoData;
  onChange?: (value?: MapGeoData) => void;
};

export const GeoDataPicker = (props: Props) => {
  const [point, setPoint] = useState<number[]>(props.value?.point ?? []);
  const [polygon, setPolygon] = useState<number[][][]>(props.value?.polygon ?? [[]]);

  useEffect(() => {
    if (props.value) {
      if (props.value.point) {
        setPoint(props.value.point);
      }
      if (props.value.polygon) {
        setPolygon(props.value.polygon);
      }
    }
  }, [props, props.value]);

  const handleChangePolygon = (coords: any) => {
    const newPolygon: number[][][] = [...polygon];
    newPolygon[0].push(coords);

    setPolygon(newPolygon);
    //Если тут БОЛЕЕ двух точек - то мы рассчитываем ЦЕНТР
    if (newPolygon[0].length > 2) {
      const center = getCenter(
        newPolygon[0].map((el) => {
          return {
            latitude: el[0],
            longitude: el[1],
          };
        })
      );

      if (!center) {
        return;
      }
      setPoint([center.latitude, center.longitude]);

      if (props.onChange) {
        props.onChange({
          point: [center.latitude, center.longitude],
          polygon: newPolygon,
        });
      }
    }
  };

  //Пользователь нажал на кнопку - очистить карту
  const clearMap = () => {
    setPoint([]);
    setPolygon([[]]);

    if (props.onChange) {
      props.onChange(undefined);
    }
  };

  function LocationMarker() {
    useMapEvents({
      click(e) {
        handleChangePolygon([e.latlng.lat, e.latlng.lng]);
      },
    });
    return (
      <>
        {point.length !== 0 && <Marker position={point as LatLngExpression} />}
        {polygon[0].length > 0 && (
          <Polygon pathOptions={{ color: 'purple' }} positions={polygon as LatLngExpression[][]}></Polygon>
        )}
      </>
    );
  }

  const onClick = (e: any) => {
    handleChangePolygon([e.lngLat.lat, e.lngLat.lng]);
  };
  //TODO - падает при изменении в update modal из-зи дупликатов
  let rightCoordinates = polygon[0].map((el) => [el[1], el[0]]);
  if (
    rightCoordinates.length > 1 &&
    rightCoordinates[0][0] !== rightCoordinates[rightCoordinates.length - 1][0] &&
    rightCoordinates[0][1] !== rightCoordinates[rightCoordinates.length - 1][1]
  ) {
    rightCoordinates = [...rightCoordinates, rightCoordinates[0]];
  }

  return (
    <>
      <Space direction="vertical">
        <Row justify="center">
          <Button type="primary" onClick={clearMap}>
            Очистить карту
          </Button>
        </Row>

        <Row justify="center">
          <MapContainer
            style={{ height: '420px', width: '500px' }}
            center={Constants.DEFAULT_MAP_CENTER as LatLngExpression}
            zoom={Constants.DEFAULT_MAP_ZOOM}
            minZoom={10}
            maxZoom={18}
            scrollWheelZoom={true}
            id="map-picker"
          >
            <MyTileLayer />
            <LocationMarker />
          </MapContainer>
        </Row>
      </Space>
    </>
  );
};

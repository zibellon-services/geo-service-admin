import { Select } from 'antd';

type Props = {
  value?: any | null;
  onChange?: (value: any) => void;
  componentType: { [s: string]: any };
};

export const SelectComponentType = (props: Props) => {
  return (
    <Select value={props.value} onChange={props.onChange} placeholder='тип'>
      {Object.entries(props.componentType).map((value) => {
        return <Select.Option key={value[0]}>{value[1]}</Select.Option>;
      })}
    </Select>
  );
};

import { Logo } from "components/logo";
import { NavBlock } from "components/nav-block";
import { observer } from "mobx-react-lite";
import { ReactNode } from "react";
import styles from "./styles.module.scss";

type Props = {
  children?: ReactNode;
};

export const BaseContainer = observer((props: Props) => {
  const { children } = props;

  return (
    <div className={styles.root}>
      <header className={styles.header}>
        <Logo></Logo>
      </header>
      <div className={styles.container}>
        <NavBlock />
        <div className={styles.content}>{children}</div>
      </div>
    </div>
  );
});

import { Row } from 'antd';
import { MyTileLayer } from 'components/my-tile-layer';
import { LatLngExpression } from 'leaflet';
import { MapContainer, Marker } from 'react-leaflet';
import { Constants } from 'utils/constants';

type Props = {
  coords: number[];
};

export const CoordsItem = (props: Props) => {
  return (
    <Row>
      <MapContainer
        center={props.coords as LatLngExpression}
        zoom={Constants.DEFAULT_MAP_ZOOM}
        minZoom={10}
        maxZoom={18}
        scrollWheelZoom={true}
        id='map-coords'
      >
        <MyTileLayer />
        {props.coords.length === 2 && <Marker position={props.coords as LatLngExpression} />}
      </MapContainer>
    </Row>
  );
};

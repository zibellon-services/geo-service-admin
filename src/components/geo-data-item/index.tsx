import { Row } from 'antd';
import { ResGeoData } from 'api';
import { MyTileLayer } from 'components/my-tile-layer';
import { LatLngExpression } from 'leaflet';
import { MapContainer, Marker, Polygon } from 'react-leaflet';
import { Constants } from 'utils/constants';

type Props = {
  geoData: ResGeoData;
};

export const GeoDataItem = (props: Props) => {
  return (
    <Row>
      <MapContainer
        center={props.geoData.point.coordinates as LatLngExpression}
        zoom={Constants.DEFAULT_MAP_ZOOM}
        minZoom={10}
        maxZoom={18}
        scrollWheelZoom={true}
        id='map-geo-data'
      >
        <MyTileLayer />
        <Polygon
          pathOptions={{ color: 'purple' }}
          positions={props.geoData.polygon.coordinates as LatLngExpression[][]}
        ></Polygon>
        {props.geoData.point.coordinates.length === 2 && (
          <Marker position={props.geoData.point.coordinates as LatLngExpression} />
        )}
      </MapContainer>
    </Row>
  );
};

import { API, ResMessageOk } from 'api';
import {
  ReqSAComponentPlaceCreate,
  ReqSAComponentPlaceUpdate,
  ReqSAPlaceSearch,
  ResSAComponentPlace,
  ResSAComponentPlaceList,
} from './classes';

const PATHS = {
  COMPONENT_PLACE: '/sa/component/place',
  COMPONENT_PLACE_ONE: '/sa/component/place/one',
};

export const SAComponentPlaceAPI = {
  getList: async (params?: ReqSAPlaceSearch) => {
    return await API.get<ResSAComponentPlaceList>({
      url: PATHS.COMPONENT_PLACE,
      params,
    });
  },
  create: async (data: ReqSAComponentPlaceCreate) => {
    return await API.post<ResSAComponentPlace>({
      url: PATHS.COMPONENT_PLACE,
      data,
    });
  },
  updateById: async (id: string, data: ReqSAComponentPlaceUpdate) => {
    return await API.patch<ResSAComponentPlace>({
      url: PATHS.COMPONENT_PLACE,
      data: {
        componentId: id,
        ...data,
      },
    });
  },

  deleteById: async (id: string) => {
    return await API.delete<ResMessageOk>({
      url: PATHS.COMPONENT_PLACE_ONE,
      params: {
        componentId: id,
      },
    });
  },
};

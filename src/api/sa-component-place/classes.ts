import { ReqGeoDataCreate, ResGeoData } from 'api';

//-----RESPONSE-----

export interface ResSAComponentPlace {
  id: string;
  name: string;
  type: string;
  geoData: ResGeoData;
  address: {
    displayName: string;
    displayDescription: string;
  };
}

export interface ResSAComponentPlaceList {
  componentPlaceList: ResSAComponentPlace[];
}

//-----REQUEST-----

export interface ReqSAComponentPlaceCreate {
  name: string;
  type: string;
  parentComponentId: string;
  geoData: ReqGeoDataCreate;
}

export interface ReqSAComponentPlaceUpdate {
  name: string;
  type: string;
  geoData: ReqGeoDataCreate;
}

export interface ReqSAPlaceSearch {
  searchValue?: string;
}

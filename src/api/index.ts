import { NetClient } from 'network/net-client';
import { getProcessEnv } from 'utils/utils-env-config';

export const API_V1_URL = `${getProcessEnv().REACT_APP_BACK_URL}/api/v1`;

export const API = new NetClient().setHost(API_V1_URL);

//-----RESPONSE-----

export interface ResMessageOk {
  message: string;
}

export interface ResGeoData {
  point: ResCoordinates;
  polygon: ResPolygon;
}

export interface ResCoordinates {
  coordinates: number[];
}

export interface ResPolygon {
  coordinates: number[][][];
}

//-----REQUEST-----

export interface ReqGeoDataCreate {
  point: ReqLatLngCreate;
  polygon: ReqLatLngCreate[];
}

export interface ReqLatLngCreate {
  lat: number;
  lng: number;
}

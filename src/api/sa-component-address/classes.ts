import { ReqGeoDataCreate, ResGeoData } from 'api';

//-----RESPONSE-----

export interface ResSAComponentAddress {
  id: string;
  name: string;
  zone: string;
  type: string;
  geoData?: ResGeoData;
  address: {
    displayName: string;
    displayDescription: string;
  };
}

export interface ResSAComponentAddressList {
  componentAddressList: ResSAComponentAddress[];
}

//-----REQUEST-----

export interface ReqSAComponentAddressCreate {
  parentComponentId?: string;
  name: string;
  zone: string;
  type: string;
  geoData?: ReqGeoDataCreate;
}

export interface ReqSAComponentAddressUpdate {
  name: string;
  type: string;
  geoData?: ReqGeoDataCreate;
}

export interface ReqSAComponentAddressSearch {
  searchValue?: string;
  parentComponentId?: string;
  zone?: string;
}

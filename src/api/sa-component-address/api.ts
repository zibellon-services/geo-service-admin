import { API, ResMessageOk } from 'api';
import {
  ReqSAComponentAddressCreate,
  ReqSAComponentAddressSearch,
  ReqSAComponentAddressUpdate,
  ResSAComponentAddress,
  ResSAComponentAddressList,
} from './classes';

const PATHS = {
  COMPONENT_ADDRESS: '/sa/component/address',
  COMPONENT_ADDRESS_ONE: '/sa/component/address/one',
};

export const SAComponentAddressAPI = {
  getList: async (params?: ReqSAComponentAddressSearch) => {
    return await API.get<ResSAComponentAddressList>({
      url: PATHS.COMPONENT_ADDRESS,
      params,
    });
  },
  getById: async (id: string) => {
    return await API.get<ResSAComponentAddress>({
      url: PATHS.COMPONENT_ADDRESS_ONE,
      params: {
        componentId: id,
      },
    });
  },
  create: async (data: ReqSAComponentAddressCreate) => {
    return await API.post<ResSAComponentAddress>({
      url: PATHS.COMPONENT_ADDRESS,
      data,
    });
  },
  updateById: async (id: string, data: ReqSAComponentAddressUpdate) => {
    return await API.patch<ResSAComponentAddress>({
      url: PATHS.COMPONENT_ADDRESS,
      data: {
        componentId: id,
        ...data,
      },
    });
  },
  deleteById: async (id: string) => {
    return await API.delete<ResMessageOk>({
      url: PATHS.COMPONENT_ADDRESS_ONE,
      params: {
        componentId: id,
      },
    });
  },
};

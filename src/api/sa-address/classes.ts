import { ResGeoData } from "api";

//-----RESPONSE-----

export interface ResSAAddress {
  id: string;
  placeId: string;
  displayName: string;
  displayDescription: string;
  type: string;
  geoData: ResGeoData;
}

export interface ResSAAddressList {
  addressList: ResSAAddress[];
}

//-----REQUEST-----

export interface ReqSASearch {
  searchValue: string;
}

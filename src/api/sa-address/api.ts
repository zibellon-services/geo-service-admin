import { API } from "api";
import { ReqSASearch, ResSAAddressList } from "./classes";

const PATHS = {
  SA_ADDRESS: "/sa/address",
  SA_ADDRESS_ID: (id: string) => `${PATHS.SA_ADDRESS}/${id}`,
};

export const SAAddressAPI = {
  getList: async (params?: ReqSASearch) => {
    return await API.get<ResSAAddressList>({
      url: PATHS.SA_ADDRESS,
      params,
    });
  },
};

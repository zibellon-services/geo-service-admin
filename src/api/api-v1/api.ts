import { API } from "api";
import {
  ReqAPIV1Autocomplete,
  ReqAPIV1Reverse,
  ReqAPIV1Route,
  ReqAPIV1Search,
  ResAPIV1Address,
  ResAPIV1AddressList,
  ResAPIV1RouteResult,
} from "./classes";

const PATHS = {
  GEOCODE: "/geocode",
  GEOCODE_REVERSE: () => `${PATHS.GEOCODE}/reverse`,
  GEOCODE_SEARCH: () => `${PATHS.GEOCODE}/search`,
  AUTOCOMPLETE: `/autocomplete`,

  ROUTE: `/routing`,
};

export const APIV1 = {
  getReverse: async (params?: ReqAPIV1Reverse) => {
    return await API.get<ResAPIV1Address>({
      url: PATHS.GEOCODE_REVERSE(),
      params,
    });
  },

  getSearch: async (params?: ReqAPIV1Search) => {
    return await API.get<ResAPIV1Address>({
      url: PATHS.GEOCODE_SEARCH(),
      params,
    });
  },

  getAutocomplete: async (params?: ReqAPIV1Autocomplete) => {
    return await API.get<ResAPIV1AddressList>({
      url: PATHS.AUTOCOMPLETE,
      params,
    });
  },

  getRoute: async (params?: ReqAPIV1Route) => {
    return await API.get<ResAPIV1RouteResult>({
      url: PATHS.ROUTE,
      params,
    });
  },
};

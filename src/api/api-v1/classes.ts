//-----RESPONSE-----

export interface ResAPIV1Component {
  zone: string;
  type: string;
  name: string;
}

export interface ResAPIV1Coordinates {
  lat: number;
  lng: number;
}

export interface ResAPIV1Address {
  displayName: string;
  displayDescription: string;
  coordinates: ResAPIV1Coordinates;
  componentList: ResAPIV1Component[];
}

export interface ResAPIV1AddressList {
  addressList: ResAPIV1Address[];
}

export interface ResAPIV1RouteResult {
  routeList: [
    {
      geometry: string;
      distance: number;
      duration: number;
    }
  ];
}

//-----REQUEST-----

export interface ReqAPIV1Autocomplete {
  searchValue: string;
}

export interface ReqAPIV1Search {
  searchValue: string;
}

export interface ReqAPIV1Reverse {
  lng: number;
  lat: number;
}

export interface ReqAPIV1Route {
  coordsString?: string;
  polylineString?: string;
}

// export interface ReqAPIV1RouteArray {
//   ReqAPIV1Route[]
// }

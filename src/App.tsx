import { BaseContainer } from 'components/base-container';
import { BaseLoader } from 'components/base-loader';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import { observer } from 'mobx-react-lite';
import { Pages } from 'pages';
import { useEffect } from 'react';
import { ToastContainer } from 'react-toastify';
import { AppStore } from 'stores/app-store';
import './App.scss';
L.Icon.Default.imagePath = 'images/';

type Props = {
  appStore: AppStore;
};

const View = ({ appStore }: Props) => {
  useEffect(() => {
    appStore.initApp();
  }, []);

  return (
    <>
      {appStore.isInit ? (
        <BaseContainer>
          <Pages />
        </BaseContainer>
      ) : (
        <BaseLoader />
      )}
      <ToastContainer position="bottom-center" autoClose={3000} hideProgressBar={false} />
    </>
  );
};
export const AppScreen = observer(View);

export const ROUTES = {
  LOGIN: {
    path: '/login',
  },
  
  AUTOCOMPLETE: {
    path: '/autocomplete',
  },
  REVERSE: {
    path: '/reverse',
  },
  SEARCH: {
    path: '/search',
  },

  WAY: {
    path: '/way',
  },
  ADDRESS: {
    path: '/address',
  },
  COMPONENT_ADDRESS: {
    path: '/component-address',
  },
  COMPONENT_PLACE: {
    path: '/component-place',
  },
}

import { MapGeoData } from "components/geo-data-picker/classes";
import { AddressComponentZoneEN } from "utils/constants";

export interface ComponentCreateFormValues {
  name: string;
  type: string;
  geoData?: MapGeoData;
}

export interface ComponentUpdateFormValues {
  name: string;
  type: string;
  geoData?: MapGeoData;
}

export interface FormItemSelect {
  label: string;
  name: string;
  required: boolean;
  zone: AddressComponentZoneEN;
}

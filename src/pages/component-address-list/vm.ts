import { SAComponentAddressAPI } from 'api/sa-component-address/api';
import { ResSAComponentAddress } from 'api/sa-component-address/classes';
import { toastInfo } from 'components/toasts/toast-info';
import { makeAutoObservable } from 'mobx';
import { AddressComponentZoneEN, AddressComponentZoneRU } from 'utils/constants';
import { mapFormGeoDataToReqLatLng } from 'utils/utils';
import { ComponentCreateFormValues, ComponentUpdateFormValues, FormItemSelect } from './classes';

export class ComponentAddressListVM {
  resComponentAddressList: ResSAComponentAddress[] = [];

  //Хотим ли мы добавить координаты, сразу при создании
  needAddCoordsForm: boolean = false;

  //Все что нужно для обновления компонента
  itemForUpdate: ResSAComponentAddress | null = null;
  isUpdateModal: boolean = false;

  createModalType: AddressComponentZoneEN | null = null;

  //Списки компонентов для выбора при создании нового
  mapResCreateComponentList = new Map<AddressComponentZoneEN, ResSAComponentAddress[]>([
    [AddressComponentZoneEN.AREA, []],
    [AddressComponentZoneEN.AREA_DETAILS, []],
    [AddressComponentZoneEN.LOCALITY, []],
    [AddressComponentZoneEN.LOCALITY_DETAILS, []],
    [AddressComponentZoneEN.LOCALITY_WAY, []],
    [AddressComponentZoneEN.HOUSE, []],
    [AddressComponentZoneEN.HOUSE_DETAILS_1, []],
    [AddressComponentZoneEN.HOUSE_DETAILS_2, []],
  ]);

  //Последний выбранный родительский компонент
  lastParentComponent: ResSAComponentAddress | null = null;

  //Список с formItemSelect - нужны для отрисовки формы в модалке СОЗДАНИЯ
  formItemSelectList: FormItemSelect[] = [];

  constructor() {
    makeAutoObservable(this);
  }

  //Все что касается модалок для создания компонентов
  openCreateModel(zone: AddressComponentZoneEN) {
    this.createModalType = zone;

    //Чистим ВСЕ списки
    this.clearCreateMapLists();

    //Инициализируем FprmItems
    this.initFormItemSelectList();

    //Если выбранная зона !== AREA -> сразу запрашиваем СПСИОК с AREA
    if (zone !== AddressComponentZoneEN.AREA) {
      this.getComponentListForCreate(AddressComponentZoneEN.AREA);
    }

    //Чистим флаг для добавления координат
    this.needAddCoordsForm = false;
  }

  closeCreateModal() {
    this.createModalType = null;

    //Чистим ВСЕ списки
    this.clearCreateMapLists();

    //очистить список для формы
    this.clearFormItemSelectList();

    //Чистим флаг для добавления координат
    this.needAddCoordsForm = false;
  }

  onChangeAddCoords(value: boolean) {
    this.needAddCoordsForm = value;
  }

  clearCreateMapLists() {
    this.mapResCreateComponentList = new Map<AddressComponentZoneEN, ResSAComponentAddress[]>([
      [AddressComponentZoneEN.AREA, []],
      [AddressComponentZoneEN.AREA_DETAILS, []],
      [AddressComponentZoneEN.LOCALITY, []],
      [AddressComponentZoneEN.LOCALITY_DETAILS, []],
      [AddressComponentZoneEN.LOCALITY_WAY, []],
      [AddressComponentZoneEN.HOUSE, []],
      [AddressComponentZoneEN.HOUSE_DETAILS_1, []],
      [AddressComponentZoneEN.HOUSE_DETAILS_2, []],
    ]);

    //Чистим объекты
    this.lastParentComponent = null;
  }

  clearFormItemSelectList() {
    this.formItemSelectList = [];
  }

  initFormItemSelectList() {
    //очистить список для формы
    this.clearFormItemSelectList();

    Object.entries(AddressComponentZoneRU).some((el) => {
      if (el[0] === this.createModalType) {
        return true;
      }

      let required = true;

      //Если мы хотим создать дом - УЛИЦА не обязательная
      //Пример - Деревня ШИШРА, 5 (В деревне НЕТ улиц)
      if (this.createModalType === AddressComponentZoneEN.HOUSE && el[1] === AddressComponentZoneRU.LOCALITY_WAY) {
        required = false;
      }

      //Если мы хотим создать уточнение дома_1 - УЛИЦА и ДОМ не обязательны
      //Пример - Город зеленоград, к22с15
      if (
        (this.createModalType === AddressComponentZoneEN.HOUSE_DETAILS_1 ||
          this.createModalType === AddressComponentZoneEN.HOUSE_DETAILS_2) &&
        (el[1] === AddressComponentZoneRU.LOCALITY_WAY || el[1] === AddressComponentZoneRU.HOUSE)
      ) {
        required = false;
      }

      this.formItemSelectList.push({
        label: el[1],
        name: el[0].toLowerCase() + '_id',
        required,
        zone: el[0] as AddressComponentZoneEN,
      });

      return false;
    });
  }

  isCreateModal() {
    return this.createModalType !== null;
  }

  getComponentListForSelect(zone: AddressComponentZoneEN): ResSAComponentAddress[] {
    return this.mapResCreateComponentList.get(zone) ?? [];
  }

  //получение списка компонентов для создания нового
  async getComponentListForCreate(zone?: AddressComponentZoneEN) {
    const resp = await SAComponentAddressAPI.getList({
      ...(this.lastParentComponent
        ? {
            parentComponentId: this.lastParentComponent.id,
          }
        : {}),
      ...(zone
        ? {
            zone,
          }
        : {}),
    });
    if (resp.isSuccess) {
      if (resp.data) {
        const tmpMap = new Map<AddressComponentZoneEN, ResSAComponentAddress[]>();

        //Все компоненты добаляем во ВРЕМЕННЫЙ MAP
        resp.data.componentAddressList
          .filter((el) => {
            return el.zone !== AddressComponentZoneEN.PLACE;
          })
          .forEach((el) => {
            const zoneEnum = el.zone as AddressComponentZoneEN;
            const mapArray = tmpMap.get(zoneEnum) ?? [];
            mapArray.push(el);
            tmpMap.set(zoneEnum, mapArray);
          });

        //Пробежка по временному МАПУ и установка данных из него в ОСНОВНОЙ
        tmpMap.forEach((el, key) => {
          this.mapResCreateComponentList.set(key, el);
        });
      }
    }
  }

  async selectParentComponentForCreate(id: string, zone: AddressComponentZoneEN) {
    this.lastParentComponent = this.mapResCreateComponentList.get(zone)!.find((el) => el.id === id)!;
    await this.getComponentListForCreate();
  }

  openUpdateModal(item: ResSAComponentAddress) {
    this.itemForUpdate = item;
    this.isUpdateModal = true;
    //Чистим флаг для добавления координат
    this.needAddCoordsForm = false;
  }

  closeUpdateModal() {
    this.itemForUpdate = null;
    this.isUpdateModal = false;
    //Чистим флаг для добавления координат
    this.needAddCoordsForm = false;
  }

  //Фильтрация
  searchValue: string = '';
  setSearchValue = (value: string) => {
    this.searchValue = value;
  };

  //Методы запросы к АПИ!
  getComponentList = async () => {
    const resp = await SAComponentAddressAPI.getList({
      searchValue: this.searchValue ?? '',
    });
    if (resp.isSuccess) {
      if (resp.data) {
        this.resComponentAddressList = resp.data?.componentAddressList;
      }
    }
  };

  private insertComponentById(item: ResSAComponentAddress) {
    const index = this.resComponentAddressList.findIndex((el) => el.id === item.id);
    if (index !== -1) {
      this.resComponentAddressList.splice(index, 1, item);
    }
  }

  createComponent = async (formValues: ComponentCreateFormValues) => {
    const resp = await SAComponentAddressAPI.create({
      ...(this.lastParentComponent
        ? {
            parentComponentId: this.lastParentComponent.id,
          }
        : {}),
      name: formValues.name,
      zone: this.createModalType ?? AddressComponentZoneEN.AREA,
      type: formValues.type,
      ...(this.needAddCoordsForm ? mapFormGeoDataToReqLatLng(formValues.geoData!) : {}),
    });

    if (resp.isSuccess) {
      toastInfo('Компонент успешно создан');
      if (resp.data) {
        this.getComponentList();
      }
    }
  };

  updateComponent = async (formValues: ComponentUpdateFormValues) => {
    if (!this.itemForUpdate) {
      return;
    }

    const resp = await SAComponentAddressAPI.updateById(this.itemForUpdate.id, {
      name: formValues.name,
      type: formValues.type,
      ...(this.needAddCoordsForm && formValues.geoData ? mapFormGeoDataToReqLatLng(formValues.geoData) : {}),
    });

    if (resp.isSuccess) {
      toastInfo('Компонент успешно обновлен');
      if (resp.data) {
        this.insertComponentById(resp.data);
      }
    }
  };

  deleteComponent = async (id: string) => {
    const resp = await SAComponentAddressAPI.deleteById(id);
    if (resp.isSuccess) {
      this.getComponentList();
    }
  };
}

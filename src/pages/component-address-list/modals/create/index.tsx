import { Button, Col, Divider, Form, Input, Modal, Row, Select, Space, Switch } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { GeoDataPicker } from 'components/geo-data-picker';
import { SelectComponentType } from 'components/selct-component-type';
import { observer } from 'mobx-react-lite';
import { ComponentCreateFormValues } from 'pages/component-address-list/classes';
import { ComponentAddressListVM } from 'pages/component-address-list/vm';
import { useUpdateEffect } from 'react-use';
import { AddressComponentEnumRU, Constants, DefaultFormProps } from 'utils/constants';
import { getFormResetFieldsByParentZone, getGeoDataFormFieldsByComponent } from 'utils/utils';

type Props = {
  vm: ComponentAddressListVM;
};

const View = ({ vm }: Props) => {
  const [form] = useForm();

  useUpdateEffect(() => {
    //Чистим ВСЕ поля, которые НИЖЕ по рангу, чем родитель-компонент
    let formClearValues = getFormResetFieldsByParentZone(vm.lastParentComponent?.zone);
    form.setFieldsValue(formClearValues);
  }, [vm, vm.lastParentComponent]);

  const onSubmit = async (values: ComponentCreateFormValues) => {
    await vm.createComponent(values);
    form.resetFields();
    vm.closeCreateModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeCreateModal();
  };

  const onChangeAddCoords = (checked: boolean) => {
    vm.onChangeAddCoords(checked);
  };

  //Выбрать координаты РОДИТЕЛЬСКОГО компонента
  const setCoordsFromParentComponent = () => {
    form.setFieldsValue(getGeoDataFormFieldsByComponent(vm.lastParentComponent));
  };

  return (
    <Modal
      title="Добавление"
      visible={vm.isCreateModal()}
      onCancel={onCancel}
      footer={false}
      width={Constants.DEFAULT_MODAL_MAP_WH}
      forceRender={true}
    >
      <Form form={form} {...DefaultFormProps} onFinish={onSubmit}>
        {vm.formItemSelectList.map((el, index) => {
          return (
            <Form.Item
              key={index}
              label={el.label}
              name={el.name}
              rules={[
                {
                  required: el.required,
                  message: 'Field is required',
                },
              ]}
            >
              <Select
                placeholder={el.label}
                onChange={(value: any) => vm.selectParentComponentForCreate(value, el.zone)}
              >
                {vm.getComponentListForSelect(el.zone).map((component) => {
                  return <Select.Option key={component.id}>{component.name}</Select.Option>;
                })}
              </Select>
            </Form.Item>
          );
        })}

        <Form.Item
          key="70"
          label="Название"
          name="name"
          rules={[
            {
              required: true,
              message: 'Field is required',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="80"
          label="Тип"
          name="type"
          rules={[
            {
              required: true,
              message: 'Field is required',
            },
          ]}
        >
          <SelectComponentType componentType={AddressComponentEnumRU} />
        </Form.Item>

        <Row justify="center">
          <Space size="middle">
            <>Добавить сразу координаты ? (Создать адрес)</>
            <Switch checked={vm.needAddCoordsForm} onChange={onChangeAddCoords} />
          </Space>
        </Row>

        <Divider />

        {vm.needAddCoordsForm && (
          <>
            <Row justify="center">
              <Col span={12}>
                <Button type="primary" block onClick={setCoordsFromParentComponent}>
                  Выбрать координаты родителя
                </Button>
              </Col>
            </Row>

            <Divider />
          </>
        )}

        {vm.needAddCoordsForm && (
          <Form.Item
            key="90"
            label="Координаты"
            name="geoData"
            rules={[
              {
                required: true,
                message: 'Field is required',
              },
            ]}
          >
            <GeoDataPicker />
          </Form.Item>
        )}

        <Form.Item key="100" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const CreateModal = observer(View);

import { Button, Divider, Form, Input, Modal, Row, Space, Switch } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { GeoDataPicker } from 'components/geo-data-picker';
import { SelectComponentType } from 'components/selct-component-type';
import { observer } from 'mobx-react-lite';
import { ComponentUpdateFormValues } from 'pages/component-address-list/classes';
import { ComponentAddressListVM } from 'pages/component-address-list/vm';
import { useUpdateEffect } from 'react-use';
import { AddressComponentEnumRU, Constants, DefaultFormProps } from 'utils/constants';
import { getFormFieldsByComponent } from 'utils/utils';

type Props = {
  vm: ComponentAddressListVM;
};

const View = ({ vm }: Props) => {
  const [form] = useForm();

  useUpdateEffect(() => {
    if (vm.itemForUpdate) {
      form.setFieldsValue(getFormFieldsByComponent(vm.itemForUpdate));
    }
  }, [vm, vm.itemForUpdate]);

  const onSubmit = async (values: ComponentUpdateFormValues) => {
    await vm.updateComponent(values);
    form.resetFields();
    vm.closeUpdateModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeUpdateModal();
  };

  const onChangeAddCoords = (checked: boolean) => {
    vm.onChangeAddCoords(checked);
  };

  return (
    <Modal
      title="Обновление"
      visible={vm.isUpdateModal}
      onCancel={onCancel}
      footer={false}
      width={Constants.DEFAULT_MODAL_MAP_WH}
      forceRender={true}
    >
      <Form form={form} {...DefaultFormProps} onFinish={onSubmit}>
        <Form.Item
          key="1"
          label="Название"
          name="name"
          rules={[
            {
              required: true,
              message: 'Field is required',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="2"
          label="Тип"
          name="type"
          rules={[
            {
              required: true,
              message: 'Field is required',
            },
          ]}
        >
          <SelectComponentType componentType={AddressComponentEnumRU} />
        </Form.Item>

        <Row justify="center">
          <Space size="middle">
            <>Добавить/Изменить/Удалить координаты</>
            <Switch checked={vm.needAddCoordsForm} onChange={onChangeAddCoords} />
          </Space>
        </Row>

        <Divider />

        {vm.needAddCoordsForm && (
          <Form.Item key="3" label="Координаты" name="geoData">
            <GeoDataPicker />
          </Form.Item>
        )}

        <Form.Item key="4" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const UpdateModal = observer(View);

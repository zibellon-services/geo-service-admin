import { Button, Input, Row, Space } from 'antd';
import { observer } from 'mobx-react-lite';
import { useEffect } from 'react';
import { AddressComponentZoneEN } from 'utils/constants';
import { ComponentListItem } from './list-item';
import { CreateModal } from './modals/create';
import { UpdateModal } from './modals/update';
import { ComponentAddressListVM } from './vm';

type Props = {
  vm: ComponentAddressListVM;
};

const View = ({ vm }: Props) => {
  useEffect(() => {
    vm.getComponentList();
  }, [vm, vm.searchValue]);

  const onSearchChange = (e: any) => {
    vm.setSearchValue(e.target.value);
  };

  const onSearch = () => {
    vm.getComponentList();
  };

  return (
    <>
      <h3>Таблица компонентов.</h3>

      <Space direction="vertical" style={{ display: 'flex' }}>
        <Input.Search
          value={vm.searchValue}
          onChange={onSearchChange}
          placeholder="Поиск..."
          onSearch={onSearch}
          enterButton
        />

        <Row align="middle">
          <Space direction="horizontal">
            <>Создать</>
            <Button type="primary" onClick={() => vm.openCreateModel(AddressComponentZoneEN.AREA)}>
              Область
            </Button>
            <Button type="primary" onClick={() => vm.openCreateModel(AddressComponentZoneEN.AREA_DETAILS)}>
              Уточнение Область
            </Button>
            <Button type="primary" onClick={() => vm.openCreateModel(AddressComponentZoneEN.LOCALITY)}>
              Поселение
            </Button>
            <Button type="primary" onClick={() => vm.openCreateModel(AddressComponentZoneEN.LOCALITY_DETAILS)}>
              Уточнение Поселение
            </Button>
            <Button type="primary" onClick={() => vm.openCreateModel(AddressComponentZoneEN.LOCALITY_WAY)}>
              Улицу
            </Button>
            <Button type="primary" onClick={() => vm.openCreateModel(AddressComponentZoneEN.HOUSE)}>
              Дом
            </Button>
            <Button type="primary" onClick={() => vm.openCreateModel(AddressComponentZoneEN.HOUSE_DETAILS_1)}>
              Уточнение 1
            </Button>
            <Button type="primary" onClick={() => vm.openCreateModel(AddressComponentZoneEN.HOUSE_DETAILS_2)}>
              Уточнение 2
            </Button>
          </Space>
        </Row>
      </Space>

      <>
        {vm.resComponentAddressList?.map((item) => {
          return <ComponentListItem key={item.id} vm={vm} item={item} />;
        })}
      </>

      <CreateModal vm={vm} />
      <UpdateModal vm={vm} />
    </>
  );
};

export const ComponentAddressListScreen = observer(View);

import { Card, Collapse, Divider, Space } from 'antd';
import { ResSAComponentAddress } from 'api/sa-component-address/classes';
import { DeleteButton } from 'components/delete-button';
import { EditButton } from 'components/edit-button';
import { GeoDataItem } from 'components/geo-data-item';
import { modalConfirm } from 'components/modals/modal-confirm';
import { observer } from 'mobx-react-lite';
import { ComponentAddressListVM } from '../vm';

type Props = {
  vm: ComponentAddressListVM;
  item: ResSAComponentAddress;
};

const View = ({ vm, item }: Props) => {
  const onEdit = () => {
    vm.openUpdateModal(item);
  };

  const onDelete = () => {
    modalConfirm({
      onOk: () => {
        vm.deleteComponent(item.id);
      },
    });
  };

  return (
    <>
      <Divider />
      <Card
        title={item.name}
        extra={
          <Space size="small">
            <EditButton onClick={onEdit} />
            <DeleteButton onClick={onDelete} />
          </Space>
        }
      >
        <Space direction="vertical" size="small">
          <div>{'Адрес: ' + item.address.displayName}</div>
          <div>{'Зона: ' + item.zone}</div>
          <div>{'Тип: ' + item.type}</div>
        </Space>

        {item.geoData && (
          <>
            <Divider />
            <Collapse>
              <Collapse.Panel header="Координаты" key="1">
                <GeoDataItem geoData={item.geoData} />
              </Collapse.Panel>
            </Collapse>
          </>
        )}
      </Card>
    </>
  );
};

export const ComponentListItem = observer(View);

export interface IPlacemarkModel {
  lat: number;
  lng: number;
}

export interface IRoute {
  geometry: string;
  distance: number;
  duration: number;
}

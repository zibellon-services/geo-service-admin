import polyline from '@mapbox/polyline';
import { Button, Card, Col, Row, Space } from 'antd';
import { MapGeoData } from 'components/geo-data-picker/classes';
import { MyTileLayer } from 'components/my-tile-layer';
import { LatLngExpression } from 'leaflet';
import { observer } from 'mobx-react-lite';
import { MapContainer, Marker, Polyline, useMapEvents } from 'react-leaflet';
import { Constants } from 'utils/constants';
import { RoutingVM } from './vm';

type Props = {
  vm: RoutingVM;
  value?: MapGeoData;
};

const View = ({ vm }: Props) => {
  const onClickAddPlacemark = (e: any) => {
    vm.addPoint(e);
  };

  const deletePlacemarks = () => {
    vm.deletePoints();
  };

  function LocationMarker() {
    useMapEvents({
      click(e) {
        onClickAddPlacemark([e.latlng.lat, e.latlng.lng]);
      },
    });
    return null;
  }

  return (
    <>
      <Row>
        <Col span={16}>
          <MapContainer
            style={{ height: '700px', width: '100%' }}
            center={Constants.DEFAULT_MAP_CENTER as LatLngExpression}
            zoom={Constants.DEFAULT_MAP_ZOOM}
            minZoom={10}
            maxZoom={18}
            scrollWheelZoom={true}
            id="map-routing"
          >
            <MyTileLayer />
            <LocationMarker />
            {vm.pointList.length > 0 &&
              vm.pointList.map((el, index) => {
                return <Marker key={index} position={[el.lat, el.lng]} />;
              })}
            {vm.routeInactiveList.length > 0 &&
              vm.routeInactiveList.map((el, index) => {
                return (
                  <Polyline
                    color={'#2b362e'}
                    key={`Route${index}`}
                    positions={polyline.decode(el.geometry) as LatLngExpression[]}
                  />
                );
              })}
            {vm.routeActive !== null && (
              <Polyline
                color={'#d9383b'}
                key={'MyRoute'}
                positions={polyline.decode(vm.routeActive.geometry) as LatLngExpression[]}
              />
            )}
          </MapContainer>
        </Col>
        <Col span={8}>
          {vm.routeList.map((el, index) => {
            return (
              <Card key={index}>
                <Space direction="vertical" size="small">
                  <>{'Дистанция(в метрах): ' + el.distance}</>
                  <>{'Время в пути(без учета пробок): ' + el.duration}</>
                </Space>
                {vm.indexActive === index ? (
                  <Button type="primary" style={{ float: 'right' }} onClick={() => vm.makeActive(index)}>
                    Выделить
                  </Button>
                ) : (
                  <Button type="default" style={{ float: 'right' }} onClick={() => vm.makeActive(index)}>
                    Выделить
                  </Button>
                )}
              </Card>
            );
          })}
          <Button type="primary" danger style={{ float: 'left' }} onClick={deletePlacemarks}>
            Удалить все
          </Button>
        </Col>
      </Row>
    </>
  );
};

export const RoutingScreen = observer(View);

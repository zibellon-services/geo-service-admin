import { APIV1 } from 'api/api-v1/api';
import { ResAPIV1Address } from 'api/api-v1/classes';
import { toastError } from 'components/toasts/toast-error';
import { makeAutoObservable } from 'mobx';
import { IPlacemarkModel, IRoute } from './classes';

export class RoutingVM {
  resAddress: ResAPIV1Address | null = null;
  pointList: IPlacemarkModel[] = [];
  routeList: IRoute[] = [];
  routeInactiveList: IRoute[] = [];
  routeActive: IRoute | null = null;
  indexActive: number = -1;

  constructor() {
    makeAutoObservable(this);
  }

  async addPoint(el: number[]) {
    if (this.pointList.length > 9) {
      toastError('Слишком много точек');
    } else {
      this.pointList.push({
        lat: el[0],
        lng: el[1],
      });
      this.routeWay();
    }
  }

  async makeActive(index: number) {
    this.indexActive = index;
    this.routeActive = this.routeList[index];
    this.routeInactiveList = this.routeList.filter((el, index1) => {
      return index1 !== index;
    });
  }

  async deletePoints() {
    this.pointList = [];
    this.routeList = [];
    this.routeInactiveList = [];
    this.routeActive = null;
    this.indexActive = -1;
  }

  async routeWay() {
    if (this.pointList.length < 2) {
      return;
    }

    const pointsString = this.pointList.map((el) => `${el.lng},${el.lat}`).join(';');

    const resp = await APIV1.getRoute({
      coordsString: pointsString,
    });

    if (resp.isSuccess) {
      if (resp.data) {
        this.routeList = resp.data.routeList;
        if (this.routeList.length > 0) {
          this.makeActive(0);
        }
      }
    }
  }
}

import { Button, Typography } from 'antd';
import { observer } from 'mobx-react-lite';
import { IPlacemarkModel } from '../classes';
import { RoutingVM } from '../vm';

type Props = {
  vm: RoutingVM;
  item: IPlacemarkModel;
  index: number;
};

const View = ({ vm, item, index }: Props) => {
  const onDelete = () => {
    // vm.deletePlacemark(item);
  };

  return (
    <>
      <Typography.Text mark>{index + 1}</Typography.Text>
      {`Координаты:[${item.lat} ; ${item.lng}]`}
      <Button onClick={onDelete}>Удалить метку</Button>
    </>
  );
};
export const RoutingPointListItem = observer(View);

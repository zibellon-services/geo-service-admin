import { APIV1 } from 'api/api-v1/api';
import { ResAPIV1Address } from 'api/api-v1/classes';
import { makeAutoObservable } from 'mobx';

export class GeocodeSearchVM {
  resAddressList: ResAPIV1Address[] = [];

  //Фильтрация
  searchValue: string = '';
  setSearchValue = (value: string) => {
    this.searchValue = value;
  };

  constructor() {
    makeAutoObservable(this);
  }

  geocodeSearch = async () => {
    const resp = await APIV1.getSearch({
      searchValue: this.searchValue ?? '',
    });
    let tmp: ResAPIV1Address[] = [];
    if (resp.isSuccess) {
      if (resp.data) {
        tmp = [resp.data];
      }
    }
    this.resAddressList = tmp;
  };
}

import { Card, Collapse, Divider, Space } from 'antd';
import { ResAPIV1Address } from 'api/api-v1/classes';
import { CoordsItem } from 'components/coords-item';
import { observer } from 'mobx-react-lite';
import { GeocodeSearchVM } from '../vm';

type Props = {
  vm: GeocodeSearchVM;
  item: ResAPIV1Address;
};

const View = ({ vm, item }: Props) => {
  return (
    <>
      <Divider />
      <Card title={item.displayName}>
        <Space direction="vertical" size="small" style={{ display: 'flex' }}>
          <div>{'Описание: ' + item.displayDescription}</div>
          <div>{`Координаты: ${item.coordinates.lat},${item.coordinates.lng}`}</div>
          <Collapse>
            <Collapse.Panel header="Координаты" key="1">
              <CoordsItem coords={[item.coordinates.lat, item.coordinates.lng]} />
            </Collapse.Panel>
          </Collapse>
        </Space>
      </Card>
    </>
  );
};

export const GeocodeSearchListItem = observer(View);

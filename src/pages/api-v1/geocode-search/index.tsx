import { Input } from 'antd';
import { observer } from 'mobx-react-lite';
import { GeocodeSearchListItem } from './list-item';
import { GeocodeSearchVM } from './vm';

type Props = {
  vm: GeocodeSearchVM;
};

const View = ({ vm }: Props) => {
  const onSearchChange = (e: any) => {
    vm.setSearchValue(e.target.value);
  };

  const onSearch = () => {
    vm.geocodeSearch();
  };

  return (
    <div>
      <h3>Поиск</h3>
      <Input.Search
        value={vm.searchValue}
        onChange={onSearchChange}
        placeholder="Поиск..."
        onSearch={onSearch}
        enterButton
      />
      <>
        {vm.resAddressList.map((item) => {
          return <GeocodeSearchListItem key={item.displayName} vm={vm} item={item} />;
        })}
      </>
    </div>
  );
};

export const GeocodeSearchScreen = observer(View);

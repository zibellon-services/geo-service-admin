import { Input } from 'antd';
import { observer } from 'mobx-react-lite';
import { useEffect, useState } from 'react';
import { useDebounce } from 'use-debounce';
import { AutoCompleteListItem } from './list-item';
import { AutoCompleteListVM } from './vm';

type Props = {
  vm: AutoCompleteListVM;
};

const View = ({ vm }: Props) => {
  const [text, setText] = useState('');
  const [debouncedText] = useDebounce(text, 500);

  useEffect(() => {
    vm.setSearchValue(debouncedText);
    vm.getAddressList();
  }, [vm, debouncedText]);

  const onSearchChange = (e: any) => {
    setText(e.target.value);
  };

  return (
    <>
      <h3>Поиск с автопродолжением</h3>
      <Input.Search value={text} style={{ width: '100%' }} onChange={onSearchChange} placeholder="Поиск..." />
      <>
        {vm.addressList?.map((item, index) => {
          return <AutoCompleteListItem key={index} vm={vm} item={item} />;
        })}
      </>
    </>
  );
};

export const AutocompleteListScreen = observer(View);

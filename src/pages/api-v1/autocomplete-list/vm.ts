import { APIV1 } from 'api/api-v1/api';
import { ResAPIV1Address } from 'api/api-v1/classes';
import { makeAutoObservable } from 'mobx';

export class AutoCompleteListVM {
  addressList: ResAPIV1Address[] = [];

  //Фильтрация
  searchValue: string = '';

  setSearchValue = (value: string) => {
    this.searchValue = value;
  };

  constructor() {
    makeAutoObservable(this);
  }

  getAddressList = async () => {
    let newList: ResAPIV1Address[] = [];
    if (this.searchValue && this.searchValue.length > 0) {
      const resp = await APIV1.getAutocomplete({
        searchValue: this.searchValue,
      });
      if (resp.isSuccess) {
        if (resp.data) {
          newList = resp.data.addressList;
        }
      }
    }
    this.addressList = newList;
  };
}

import { Card, Collapse, Space } from 'antd';
import { ResAPIV1Address } from 'api/api-v1/classes';
import { observer } from 'mobx-react-lite';

type Props = {
  item: ResAPIV1Address;
};

const View = ({ item }: Props) => {
  return (
    <Card style={{ width: '95%' }} title={item.displayName}>
      <Space style={{ width: '100%' }} direction='vertical' size='small'>
        <div>{'Описание: ' + item.displayDescription}</div>
        <div>{`Координаты: ${item.coordinates.lat},${item.coordinates.lng}`}</div>
        <Collapse>
          {item.componentList.map((el, index) => {
            return (
              <Collapse.Panel header={el.zone} key={index}>
                <div>{'Название: ' + el.name}</div>
                <div>{'Тип: ' + el.type}</div>
              </Collapse.Panel>
            );
          })}
        </Collapse>
      </Space>
    </Card>
  );
};

export const Item = observer(View);

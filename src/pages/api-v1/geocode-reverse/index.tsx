import { Col, Row } from 'antd';
import { MapGeoData } from 'components/geo-data-picker/classes';
import { MyTileLayer } from 'components/my-tile-layer';
import { LatLngExpression } from 'leaflet';
import { observer } from 'mobx-react-lite';
import { MapContainer, Marker, useMapEvents } from 'react-leaflet';
import { Constants } from 'utils/constants';
import { Item } from './list-item';
import { GeocodeReverseVM } from './vm';

type Props = {
  vm: GeocodeReverseVM;
  value?: MapGeoData;
};

const View = ({ vm }: Props) => {
  //todo zoom  to all maps
  function LocationMarker() {
    useMapEvents({
      click(e) {
        vm.selectPoint([e.latlng.lat, e.latlng.lng]);
      },
    });
    return <>{vm.point.length !== 0 && <Marker position={vm.point as LatLngExpression} />}</>;
  }

  return (
    <>
      <Row>
        <Col span={16}>
          <MapContainer
            style={{ height: '800px', width: '100%' }}
            center={Constants.DEFAULT_MAP_CENTER as LatLngExpression}
            zoom={Constants.DEFAULT_MAP_ZOOM}
            minZoom={10}
            maxZoom={18}
            scrollWheelZoom={true}
            id="map-reverse"
          >
            <MyTileLayer />
            <LocationMarker />
          </MapContainer>
        </Col>
        <Col span={8}>{vm.resAddress && <Item item={vm.resAddress} />}</Col>
      </Row>
    </>
  );
};

export const GeocodeReverseScreen = observer(View);

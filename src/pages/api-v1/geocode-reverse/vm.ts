import { APIV1 } from 'api/api-v1/api';
import { ResAPIV1Address } from 'api/api-v1/classes';
import { makeAutoObservable } from 'mobx';

export class GeocodeReverseVM {
  resAddress: ResAPIV1Address | null = null;
  point: number[] = [];

  constructor() {
    makeAutoObservable(this);
  }

  async selectPoint(el: number[]) {
    this.point = el;

    const resp = await APIV1.getReverse({
      lat: el[0],
      lng: el[1],
    });

    let tmp: ResAPIV1Address | null = null;
    if (resp.isSuccess) {
      if (resp.data) {
        tmp = resp.data;
      }
    }
    this.resAddress = tmp;
  }
}

import { Button, Col, Divider, Form, Input, Modal, Row, Select, Space, Switch } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import { GeoDataPicker } from 'components/geo-data-picker';
import { SelectComponentType } from 'components/selct-component-type';
import { observer } from 'mobx-react-lite';
import { PlaceCreateFormValues } from 'pages/component-place-list/classes';
import { ComponentPlaceListVM } from 'pages/component-place-list/vm';
import { useUpdateEffect } from 'react-use';
import { AddressComponentEnumRU, AddressComponentZoneEN, Constants, DefaultFormProps } from 'utils/constants';
import { getFormResetFieldsByParentZone, getGeoDataFormFieldsByComponent } from 'utils/utils';

type Props = {
  vm: ComponentPlaceListVM;
};

const View = ({ vm }: Props) => {
  const [form] = useForm();

  useUpdateEffect(() => {
    //Чистим ВСЕ поля, которые НИЖЕ по рангу, чем родитель-компонент

    let formClearValues = getFormResetFieldsByParentZone(vm.lastParentComponent?.zone);

    form.setFieldsValue(formClearValues);
  }, [vm, vm.lastParentComponent]);

  const onSubmit = async (values: PlaceCreateFormValues) => {
    await vm.createPlace(values);
    form.resetFields();
    vm.closeCreateModal();
  };

  const onCancel = () => {
    form.resetFields();
    vm.closeCreateModal();
  };

  const selectParentComponentId = (value: any, zone: AddressComponentZoneEN) => {
    vm.selectParentComponentForCreate(value, zone);
  };

  const setCoordsFromParentComponent = () => {
    form.setFieldsValue(getGeoDataFormFieldsByComponent(vm.lastParentComponent));
  };

  const onChangeAddCoords = (checked: boolean) => {
    vm.onChangeAddCoords(checked);
  };

  return (
    <Modal
      title="Добавление"
      visible={vm.isCreateModal}
      onCancel={onCancel}
      footer={false}
      width={Constants.DEFAULT_MODAL_MAP_WH}
      forceRender={true}
    >
      <Form form={form} {...DefaultFormProps} onFinish={onSubmit}>
        <Form.Item
          key="1"
          label="Округ/Область"
          name="areaId"
          rules={[
            {
              required: true,
              message: 'Field is required',
            },
          ]}
        >
          <Select
            placeholder="Округ/Область"
            onChange={(value: any) => selectParentComponentId(value, AddressComponentZoneEN.AREA)}
          >
            {vm.getComponentListForSelect(AddressComponentZoneEN.AREA).map((el) => {
              return <Select.Option key={el.id}>{el.name}</Select.Option>;
            })}
          </Select>
        </Form.Item>

        <Form.Item key="2" label="Уточнение Округ/Область" name="areaDetailsId">
          <Select
            placeholder="Уточнение Округ/Область"
            onChange={(value: any) => selectParentComponentId(value, AddressComponentZoneEN.AREA_DETAILS)}
          >
            {vm.getComponentListForSelect(AddressComponentZoneEN.AREA_DETAILS).map((el) => {
              return <Select.Option key={el.id}>{el.name}</Select.Option>;
            })}
          </Select>
        </Form.Item>

        <Form.Item key="3" label="Поселение" name="localityId">
          <Select
            placeholder="Поселение"
            onChange={(value: any) => selectParentComponentId(value, AddressComponentZoneEN.LOCALITY)}
          >
            {vm.getComponentListForSelect(AddressComponentZoneEN.LOCALITY).map((el) => {
              return <Select.Option key={el.id}>{el.name}</Select.Option>;
            })}
          </Select>
        </Form.Item>

        <Form.Item key="4" label="Уточнение Поселение" name="localityDetailsId">
          <Select
            placeholder="Уточнение Поселение"
            onChange={(value: any) => selectParentComponentId(value, AddressComponentZoneEN.LOCALITY_DETAILS)}
          >
            {vm.getComponentListForSelect(AddressComponentZoneEN.LOCALITY_DETAILS).map((el) => {
              return <Select.Option key={el.id}>{el.name}</Select.Option>;
            })}
          </Select>
        </Form.Item>

        <Form.Item key="5" label="Улица" name="localityWayId">
          <Select
            placeholder="Улица"
            onChange={(value: any) => selectParentComponentId(value, AddressComponentZoneEN.LOCALITY_WAY)}
          >
            {vm.getComponentListForSelect(AddressComponentZoneEN.LOCALITY_WAY).map((el) => {
              return <Select.Option key={el.id}>{el.name}</Select.Option>;
            })}
          </Select>
        </Form.Item>

        <Form.Item key="6" label="Дом" name="houseId">
          <Select
            placeholder="Дом"
            onChange={(value: any) => selectParentComponentId(value, AddressComponentZoneEN.HOUSE)}
          >
            {vm.getComponentListForSelect(AddressComponentZoneEN.HOUSE).map((el) => {
              return <Select.Option key={el.id}>{el.name}</Select.Option>;
            })}
          </Select>
        </Form.Item>

        <Form.Item key="7" label="Уточнение_1" name="houseDetails1Id">
          <Select
            placeholder="Уточнение_1"
            onChange={(value: any) => selectParentComponentId(value, AddressComponentZoneEN.HOUSE_DETAILS_1)}
          >
            {vm.getComponentListForSelect(AddressComponentZoneEN.HOUSE_DETAILS_1).map((el) => {
              return <Select.Option key={el.id}>{el.name}</Select.Option>;
            })}
          </Select>
        </Form.Item>

        <Form.Item key="8" label="Уточнение_2" name="houseDetails2Id">
          <Select
            placeholder="Уточнение_2"
            onChange={(value: any) => selectParentComponentId(value, AddressComponentZoneEN.HOUSE_DETAILS_2)}
          >
            {vm.getComponentListForSelect(AddressComponentZoneEN.HOUSE_DETAILS_2).map((el) => {
              return <Select.Option key={el.id}>{el.name}</Select.Option>;
            })}
          </Select>
        </Form.Item>

        <Form.Item
          label="Название"
          name="name"
          key="9"
          rules={[
            {
              required: true,
              message: 'Field is required',
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          key="10"
          label="Тип"
          name="type"
          rules={[
            {
              required: true,
              message: 'Field is required',
            },
          ]}
        >
          <SelectComponentType componentType={AddressComponentEnumRU} />
        </Form.Item>

        <Row justify="center">
          <Space size="middle">
            <>Карта</>
            <Switch checked={vm.needAddCoordsForm} onChange={onChangeAddCoords} />
          </Space>
        </Row>
        <Divider />
        {vm.needAddCoordsForm && (
          <>
            <Row justify="center">
              <Col span={12}>
                <Button type="primary" block onClick={setCoordsFromParentComponent}>
                  Выбрать координаты родителя
                </Button>
              </Col>
            </Row>

            <Divider />
          </>
        )}

        {vm.needAddCoordsForm && (
          <Form.Item
            key="11"
            label="Координаты"
            name="geoData"
            rules={[
              {
                required: true,
                message: 'Field is required',
              },
            ]}
          >
            {vm.isMap ? <GeoDataPicker /> : ''}
          </Form.Item>
        )}
        <Form.Item key="12" wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            Сохранить
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export const ComponentPlaceCreateModal = observer(View);

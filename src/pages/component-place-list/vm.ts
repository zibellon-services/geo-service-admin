import { SAComponentAddressAPI } from 'api/sa-component-address/api';
import { ResSAComponentAddress } from 'api/sa-component-address/classes';
import { SAComponentPlaceAPI } from 'api/sa-component-place/api';
import { ResSAComponentPlace } from 'api/sa-component-place/classes';
import { toastInfo } from 'components/toasts/toast-info';
import { makeAutoObservable } from 'mobx';
import { AddressComponentZoneEN } from 'utils/constants';
import { mapFormGeoDataToReqLatLng } from 'utils/utils';
import { PlaceCreateFormValues, PlaceUpdateFormValues } from './classes';

export class ComponentPlaceListVM {
  resPlaceList: ResSAComponentPlace[] = [];
  isCreateModal: boolean = false;
  itemForUpdate: ResSAComponentPlace | null = null;
  isUpdateModal: boolean = false;
  isMap: boolean = false;
  //Списки компонентов для выбора при создании нового

  //Хотим ли мы добавить координаты, сразу при создании
  needAddCoordsForm: boolean = false;

  mapResCreateComponentList = new Map<AddressComponentZoneEN, ResSAComponentAddress[]>([
    [AddressComponentZoneEN.AREA, []],
    [AddressComponentZoneEN.AREA_DETAILS, []],
    [AddressComponentZoneEN.LOCALITY, []],
    [AddressComponentZoneEN.LOCALITY_DETAILS, []],
    [AddressComponentZoneEN.LOCALITY_WAY, []],
    [AddressComponentZoneEN.HOUSE, []],
    [AddressComponentZoneEN.HOUSE_DETAILS_1, []],
    [AddressComponentZoneEN.HOUSE_DETAILS_2, []],
  ]);

  lastParentComponent: ResSAComponentAddress | null = null;

  //Фильтрация
  searchValue: string = '';
  setSearchValue = (value: string) => {
    this.searchValue = value;
  };

  constructor() {
    makeAutoObservable(this);
  }

  clearCreateLists() {
    //Чистим списки
    this.mapResCreateComponentList = new Map<AddressComponentZoneEN, ResSAComponentAddress[]>([
      [AddressComponentZoneEN.AREA, []],
      [AddressComponentZoneEN.AREA_DETAILS, []],
      [AddressComponentZoneEN.LOCALITY, []],
      [AddressComponentZoneEN.LOCALITY_DETAILS, []],
      [AddressComponentZoneEN.LOCALITY_WAY, []],
      [AddressComponentZoneEN.HOUSE, []],
      [AddressComponentZoneEN.HOUSE_DETAILS_1, []],
      [AddressComponentZoneEN.HOUSE_DETAILS_2, []],
    ]);

    //Чистим объекты
    this.lastParentComponent = null;
  }

  onChangeAddCoords(value: boolean) {
    this.needAddCoordsForm = value;
  }

  getComponentListForSelect(zone: AddressComponentZoneEN): ResSAComponentAddress[] {
    return this.mapResCreateComponentList.get(zone) ?? [];
  }

  async getComponentListForCreate(zone?: AddressComponentZoneEN) {
    const resp = await SAComponentAddressAPI.getList({
      ...(this.lastParentComponent
        ? {
            parentComponentId: this.lastParentComponent.id,
          }
        : {}),
      ...(zone
        ? {
            zone,
          }
        : {}),
    });
    if (resp.isSuccess) {
      if (resp.data) {
        const tmpMap = new Map<AddressComponentZoneEN, ResSAComponentAddress[]>();

        //Все компоненты добаляем во ВРЕМЕННЫЙ MAP
        resp.data.componentAddressList.forEach((el) => {
          const zoneEnum = el.zone as AddressComponentZoneEN;
          const mapArray = tmpMap.get(zoneEnum) ?? [];
          mapArray.push(el);
          tmpMap.set(zoneEnum, mapArray);
        });

        //Пробежка по временному МАПУ и установка данных из него в ОСНОВНОЙ
        tmpMap.forEach((el, key) => {
          this.mapResCreateComponentList.set(key, el);
        });
      }
    }
  }

  async selectParentComponentForCreate(id: string, zone: AddressComponentZoneEN) {
    this.lastParentComponent = this.mapResCreateComponentList.get(zone)!.find((el) => el.id === id)!;
    await this.getComponentListForCreate();
  }

  async openCreateModal() {
    this.isCreateModal = true;

    //Чистим ВСЕ списки
    this.clearCreateLists();

    this.getComponentListForCreate(AddressComponentZoneEN.AREA);
    await new Promise((r) => setTimeout(r, 0));
    this.isMap = true;
  }

  closeCreateModal() {
    this.isCreateModal = false;
    this.clearCreateLists();
    this.isMap = false;
    this.needAddCoordsForm = false;
  }

  openUpdateModal(item: ResSAComponentPlace) {
    this.itemForUpdate = item;
    this.isUpdateModal = true;
  }

  closeUpdateModal() {
    this.itemForUpdate = null;
    this.isUpdateModal = false;
    this.needAddCoordsForm = false;
  }

  getPlaceList = async () => {
    const resp = await SAComponentPlaceAPI.getList();
    if (resp.isSuccess) {
      if (resp.data) {
        this.resPlaceList = resp.data.componentPlaceList;
      }
    }
  };

  private insertPlaceById(item: ResSAComponentPlace) {
    const index = this.resPlaceList.findIndex((el) => el.id === item.id);
    if (index !== -1) {
      this.resPlaceList.splice(index, 1, item);
    }
  }

  createPlace = async (formValues: PlaceCreateFormValues) => {
    if (!this.lastParentComponent) {
      return;
    }

    const createPlaceData = {
      name: formValues.name,
      type: formValues.type,
      parentComponentId: this.lastParentComponent.id,
      ...mapFormGeoDataToReqLatLng(formValues.geoData),
    };

    const resp = await SAComponentPlaceAPI.create(createPlaceData);

    if (resp.isSuccess) {
      toastInfo('Компонент успешно создан');
      if (resp.data) {
        this.getPlaceList();
      }
    }
  };

  updatePlace = async (formValues: PlaceUpdateFormValues) => {
    if (!this.itemForUpdate) {
      return;
    }

    const resp = await SAComponentPlaceAPI.updateById(this.itemForUpdate.id, {
      name: formValues.name,
      type: formValues.type,
      ...mapFormGeoDataToReqLatLng(formValues.geoData),
    });

    if (resp.isSuccess) {
      if (resp.data) {
        this.insertPlaceById(resp.data);
      }
    }
  };

  deletePlace = async (id: string) => {
    const resp = await SAComponentPlaceAPI.deleteById(id);
    if (resp.isSuccess) {
      const index = this.resPlaceList.findIndex((el) => el.id === id);
      if (index !== -1) {
        this.resPlaceList.splice(index, 1);
      }
    }
  };
}

import { Card, Collapse, Divider, Space } from 'antd';
import { ResSAComponentPlace } from 'api/sa-component-place/classes';
import { DeleteButton } from 'components/delete-button';
import { EditButton } from 'components/edit-button';
import { GeoDataItem } from 'components/geo-data-item';
import { modalConfirm } from 'components/modals/modal-confirm';
import { observer } from 'mobx-react-lite';
import { AddressComponentEnumRU } from 'utils/constants';
import { mapKeyToVal } from 'utils/utils';
import { ComponentPlaceListVM } from '../vm';

type Props = {
  vm: ComponentPlaceListVM;
  item: ResSAComponentPlace;
};

const View = ({ vm, item }: Props) => {
  const onEdit = () => {
    vm.openUpdateModal(item);
  };

  const onDelete = () => {
    modalConfirm({
      onOk: () => {
        vm.deletePlace(item.id);
      },
    });
  };

  return (
    <>
      <Divider />
      <Card
        title={'Название: ' + item.name}
        extra={
          <Space size="middle">
            <EditButton onClick={onEdit} />
            <DeleteButton onClick={onDelete} />
          </Space>
        }
      >
        <Space size="small" direction="vertical">
          <div>{'Адрес: ' + item.address.displayDescription}</div>
          <div>{'Тип: ' + mapKeyToVal(item.type, AddressComponentEnumRU)}</div>
        </Space>

        <Divider />

        <Collapse>
          <Collapse.Panel header="Координаты" key="2">
            <GeoDataItem geoData={item.geoData} />
          </Collapse.Panel>
        </Collapse>
      </Card>
    </>
  );
};

export const ComponentPlaceListItem = observer(View);

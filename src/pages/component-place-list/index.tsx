import { Button, Input, Row, Space } from 'antd';
import { observer } from 'mobx-react-lite';
import { useEffect } from 'react';
import { ComponentPlaceListItem } from './list-item';
import { ComponentPlaceCreateModal } from './modals/create';
import { ComponentPlaceUpdateModal } from './modals/update';
import { ComponentPlaceListVM } from './vm';

type Props = {
  vm: ComponentPlaceListVM;
};

const View = ({ vm }: Props) => {
  useEffect(() => {
    vm.getPlaceList();
  }, [vm, vm.searchValue]);

  const onSearchChange = (e: any) => {
    vm.setSearchValue(e.target.value);
  };

  const onSearch = () => {
    vm.getPlaceList();
  };

  return (
    <div>
      <h3>Места интереса.</h3>

      <Space direction="vertical" style={{ display: 'flex' }}>
        <Input.Search
          value={vm.searchValue}
          onChange={onSearchChange}
          placeholder="Поиск..."
          onSearch={onSearch}
          enterButton
        />

        <Row>
          <Button type="primary" onClick={() => vm.openCreateModal()}>
            Создать место интереса
          </Button>
        </Row>
      </Space>

      <>
        {vm.resPlaceList?.map((item) => {
          return <ComponentPlaceListItem key={item.id} vm={vm} item={item} />;
        })}
      </>

      <ComponentPlaceCreateModal vm={vm} />
      <ComponentPlaceUpdateModal vm={vm} />
    </div>
  );
};

export const ComponentPlaceListScreen = observer(View);

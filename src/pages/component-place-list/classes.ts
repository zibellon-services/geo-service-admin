import { MapGeoData } from 'components/geo-data-picker/classes';

export type PlaceCreateFormValues = {
  name: string;
  type: string;
  geoData: MapGeoData;
};

export type PlaceUpdateFormValues = {
  name: string;
  type: string;
  geoData: MapGeoData;
};

import { Input } from 'antd';
import { observer } from 'mobx-react-lite';
import { useEffect } from 'react';
import { AddressListItem } from './list-item';
import styles from './styles.module.scss';
import { AddressListVM } from './vm';

type Props = {
  vm: AddressListVM;
};

const View = ({ vm }: Props) => {
  useEffect(() => {
    vm.getAddressList();
  }, [vm, vm.searchValue]);

  const onSearchChange = (e: any) => {
    vm.setSearchValue(e.target.value);
  };

  const onSearch = () => {
    vm.getAddressList();
  };

  return (
    <div className={styles.root}>
      <h3>Адреса</h3>
      <Input.Search
        value={vm.searchValue}
        onChange={onSearchChange}
        className={styles.search}
        placeholder="Поиск..."
        onSearch={onSearch}
        enterButton
      />
      {/* <Row>
        <Select placeholder="Округ">
          {vm.addressList?.map((address) => {
            return <Select.Option key={address.id}>{address}</Select.Option>;
          })}
        </Select>
        <Select placeholder="Город">
          {vm.addressList?.map((stack) => {
            return <Select.Option key={stack.id}>{stack}</Select.Option>;
          })}
        </Select>
        <Select placeholder="Улица">
          {vm.addressList?.map((stack) => {
            return <Select.Option key={stack.id}>{stack}</Select.Option>;
          })}
        </Select>
        <Select placeholder="Дом">
          {vm.addressList?.map((stack) => {
            return <Select.Option key={stack.id}>{stack}</Select.Option>;
          })}
        </Select>
        <Select placeholder="Уточнение">
          {vm.addressList?.map((stack) => {
            return <Select.Option key={stack.id}>{stack}</Select.Option>;
          })}
        </Select>
      </Row> */}

      <>
        {vm.addressList.map((item) => {
          return <AddressListItem key={item.id} vm={vm} item={item} />;
        })}
      </>
    </div>
  );
};

export const AddressListScreen = observer(View);

import { SAAddressAPI } from 'api/sa-address/api';
import { ResSAAddress } from 'api/sa-address/classes';
import { makeAutoObservable } from 'mobx';

export class AddressListVM {
  addressList: ResSAAddress[] = [];

  //Фильтрация
  searchValue: string = '';
  setSearchValue = (value: string) => {
    this.searchValue = value;
  };

  constructor() {
    makeAutoObservable(this);
  }

  getAddressList = async () => {
    const resp = await SAAddressAPI.getList({
      searchValue: this.searchValue ?? '',
    });
    if (resp.isSuccess) {
      if (resp.data) {
        this.addressList = resp.data.addressList;
      }
    }
  };
}

import { Card, Collapse, Divider, Space } from 'antd';
import { ResSAAddress } from 'api/sa-address/classes';
import { GeoDataItem } from 'components/geo-data-item';
import { observer } from 'mobx-react-lite';
import { AddressListVM } from '../vm';
import styles from './styles.module.scss';

type Props = {
  vm: AddressListVM;
  item: ResSAAddress;
};

const View = ({ vm, item }: Props) => {
  return (
    <>
      <Card className={styles.card} title={item.displayName}>
        <Space direction="vertical" size="small">
          <div>{'Описание: ' + item.displayDescription}</div>
          <div>{'Тип: ' + item.type}</div>
        </Space>
        {item.geoData && (
          <>
            <Divider />
            <Collapse>
              <Collapse.Panel header="Координаты" key="1">
                <GeoDataItem geoData={item.geoData} />
              </Collapse.Panel>
            </Collapse>
          </>
        )}
      </Card>
    </>
  );
};

export const AddressListItem = observer(View);

import { observer } from 'mobx-react-lite';
import React from 'react';
import { Route } from 'react-router-dom';
import { ROUTES } from 'router';
import { AddressListScreen } from './address-list';
import { AddressListVM } from './address-list/vm';
import { AutocompleteListScreen } from './api-v1/autocomplete-list';
import { AutoCompleteListVM } from './api-v1/autocomplete-list/vm';
import { GeocodeReverseScreen } from './api-v1/geocode-reverse';
import { GeocodeReverseVM } from './api-v1/geocode-reverse/vm';
import { GeocodeSearchScreen } from './api-v1/geocode-search';
import { GeocodeSearchVM } from './api-v1/geocode-search/vm';
import { RoutingScreen } from './api-v1/routing';
import { RoutingVM } from './api-v1/routing/vm';
import { ComponentAddressListScreen } from './component-address-list';
import { ComponentAddressListVM } from './component-address-list/vm';
import { ComponentPlaceListScreen } from './component-place-list';
import { ComponentPlaceListVM } from './component-place-list/vm';

export const Pages = observer(() => {
  return (
    <>
      {/* CLIENT ZONE */}

      <Route exact path={ROUTES.AUTOCOMPLETE.path}>
        <AutocompleteListScreen vm={new AutoCompleteListVM()} />
      </Route>
      <Route exact path={ROUTES.SEARCH.path}>
        <GeocodeSearchScreen vm={new GeocodeSearchVM()} />
      </Route>
      <Route exact path={ROUTES.REVERSE.path}>
        <GeocodeReverseScreen vm={new GeocodeReverseVM()} />
      </Route>
      <Route exact path={ROUTES.WAY.path}>
        <RoutingScreen vm={new RoutingVM()} />
      </Route>

      {/* SA ZONE */}

      <Route exact path={ROUTES.ADDRESS.path}>
        <AddressListScreen vm={new AddressListVM()} />
      </Route>
      <Route exact path={ROUTES.COMPONENT_ADDRESS.path}>
        <ComponentAddressListScreen vm={new ComponentAddressListVM()} />
      </Route>
      <Route exact path={ROUTES.COMPONENT_PLACE.path}>
        <ComponentPlaceListScreen vm={new ComponentPlaceListVM()} />
      </Route>
    </>
  );
});

import { API } from 'api';
import { toastError } from 'components/toasts/toast-error';
import { makeAutoObservable } from 'mobx';

export class AppStore {
  isInit: boolean = false;

  constructor() {
    makeAutoObservable(this);
  }

  initApp() {
    API.setOnRequestError(async (error: any) => {
      toastError(error.message);
      return false;
    });
    this.isInit = true;
  }
}

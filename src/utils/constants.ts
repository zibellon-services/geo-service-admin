export enum AddressComponentZoneEN {
  AREA = 'AREA', //округ
  AREA_DETAILS = 'AREA_DETAILS', //Некоторое уточнение для AREA
  LOCALITY = 'LOCALITY', // город деревня село снт
  LOCALITY_DETAILS = 'LOCALITY_DETAILS', // УТОЧНЕНИЕ для: город деревня село снт
  LOCALITY_WAY = 'LOCALITY_WAY', //улица бульвар площадь
  HOUSE = 'HOUSE', //дом
  HOUSE_DETAILS_1 = 'HOUSE_DETAILS_1', //уточнения
  HOUSE_DETAILS_2 = 'HOUSE_DETAILS_2',
  PLACE = 'PLACE', //МЕСТО
}

export enum AddressComponentZoneRU {
  AREA = 'округ/район', //округ
  AREA_DETAILS = 'уточнение округ/район',
  LOCALITY = 'поселение', // город, деревня, село,снт
  LOCALITY_DETAILS = 'уточнение поселения',
  LOCALITY_WAY = 'улица', //улица бульвар площадь
  HOUSE = 'дом', //дом
  HOUSE_DETAILS_1 = 'уточнение1', //уточнения
  HOUSE_DETAILS_2 = 'уточнение2',
}

export enum AddressComponentTypeAreaRU {
  COUNTY = 'округ',
  CITY_COUNTY = 'городской округ',
  AREA = 'район',
  REGION = 'область',
  EDGE = 'край',
  REPUBLIC = 'республика',
}

export enum AddressComponentTypeLocalityRU {
  CITY = 'город',
  VILLAGE = 'деревня',
  SUBURB = 'микрорайон',
  HAMLET = 'поселок',
  SELO = 'село',
  COUNTY = 'округ',
  RURAL_SETTELMENT = 'сельское поселение',
  COTTAGE_SETTELMENT = 'коттеджный поселок',
  ST = 'СТ',
  SNT = 'СНТ',
  SOT = 'СОТ',
  SONT = 'СОНТ',
  KIZ = 'КИЗ',
  TSO = 'ТСО',
  TIZ = 'ТИЗ',
}

export enum AddressComponentTypeLocalityWayRU {
  STREET = 'улица',
  LINE = 'линия',
  ROADWAY = 'шоссе',
  SQUARE = 'площадь',
  PARK = 'парк',
  AVENUE = 'проспект',
  TRAVEL = 'проезд',
  LANE = 'переулок',
  BOULEVARD = 'бульвар',
  EMBANKMENT = 'набережная',
}

export enum AddressComponentTypeHouseRU {
  HOUSE = 'дом',
}

export enum AddressComponentTypeHouseDetailRU {
  STRUCTURE = 'строение',
  CORPUS = 'корпус',
  ANNEX = 'пристройка',
}

export enum AddressComponentTypePlaceRU {
  SIMPLE_PLACE = 'просто место',
  AIRPORT = 'аэропорт', //
  SHOP = 'магазин', //
  SPORT = 'спорт',
  TOURISM = 'туризм', //
  HISTORIC = 'историческое', //
  RAILWAY = 'Ж/Д', //
  AMENITY = 'удобства', //
  JOYANDREST = 'развлечение и отдых', //
  LANDUSE = 'зарезервированная территория', //
  NATURAL = 'природа',
  OFFICE = 'офис',
  WATERWAY = 'прибрежная зона',
  CRAFT = 'ручная работа',
  MAN_MADE = 'рукотворный объект',
  HIGHWAY = 'что-то на шоссе',
  BUILDING = 'здание',
  INDUSTRIAL = 'промышленный район',
}

export const AddressComponentEnumRU = {
  ...AddressComponentTypeAreaRU,
  ...AddressComponentTypeLocalityRU,
  ...AddressComponentTypeLocalityWayRU,
  ...AddressComponentTypeHouseRU,
  ...AddressComponentTypeHouseDetailRU,
  ...AddressComponentTypePlaceRU,
};
export type AddressComponentTypeRU = typeof AddressComponentEnumRU;

export class Constants {
  public static DEFAULT_MAP_CENTER: number[] = [64.54173446122648, 40.533422031636995];
  public static DEFAULT_MAP_CENTER_LNG: number = 40.533422031636995;
  public static DEFAULT_MAP_CENTER_LAT: number = 64.54173446122648;

  public static DEFAULT_MAP_STYLE: string =
    'https://api.maptiler.com/maps/d4cefefd-79f4-419b-b042-6cb16ae66ffa/style.json?key=u7XT520T5ZnhaPdUN5XU';
  public static DEFAULT_MAP_ZOOM: number = 13;
  public static DEFAULT_MODAL_MAP_WH: number = 1300;
  public static TILE_SERVER_URL: string = `https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png`;
}

export const DefaultFormProps = {
  name: 'basic',
  autoComplete: 'off',
  labelCol: { span: 5 },
  wrapperCol: { span: 17 },
};

import { ReqGeoDataCreate, ResGeoData } from "api";
import { ResSAComponentAddress } from "api/sa-component-address/classes";
import { ResSAComponentPlace } from "api/sa-component-place/classes";
import { MapGeoData } from "components/geo-data-picker/classes";
import {
  AddressComponentTypeAreaRU,
  AddressComponentTypeHouseDetailRU,
  AddressComponentTypeHouseRU,
  AddressComponentTypeLocalityRU,
  AddressComponentTypeLocalityWayRU,
  AddressComponentZoneEN
} from "./constants";

export const getTypeEnumRUforZone = (zoneEN: string): { [s: string]: any } => {
  switch (zoneEN) {
    case AddressComponentZoneEN.AREA:
      return AddressComponentTypeAreaRU;

    case AddressComponentZoneEN.LOCALITY:
      return AddressComponentTypeLocalityRU;

    case AddressComponentZoneEN.LOCALITY_WAY:
      return AddressComponentTypeLocalityWayRU;

    case AddressComponentZoneEN.HOUSE:
      return AddressComponentTypeHouseRU;

    case AddressComponentZoneEN.HOUSE_DETAILS_1:
    case AddressComponentZoneEN.HOUSE_DETAILS_2:
      return AddressComponentTypeHouseDetailRU;
    default:
      return {};
  }
};

export const getFormResetFieldsByParentZone = (zone?: string): { [s: string]: any } => {
  let formClearValues = {};

  //Если мы поменяли AREA
  if (zone === AddressComponentZoneEN.AREA) {
    //Сбрасываем LOCALITY | LOCALITY_WAY | HOUSE | HOUSE_DETAILS_1
    formClearValues = {
      localityId: null,
      localityWayId: null,
      houseId: null,
      houseDetails1Id: null,
    };
  }

  if (zone === AddressComponentZoneEN.LOCALITY) {
    //Сбрасываем LOCALITY_WAY | HOUSE | HOUSE_DETAILS_1
    formClearValues = {
      localityWayId: null,
      houseId: null,
      houseDetails1Id: null,
    };
  }

  if (zone === AddressComponentZoneEN.LOCALITY_WAY) {
    //Сбрасываем HOUSE | HOUSE_DETAILS_1
    formClearValues = {
      houseId: null,
      houseDetails1Id: null,
    };
  }

  if (zone === AddressComponentZoneEN.HOUSE) {
    //Сбрасываем HOUSE_DETAILS_1
    formClearValues = {
      houseDetails1Id: null,
    };
  }

  return formClearValues;
};

//Преобразование ПРАВОГО значения (РУССКОЕ/ENGLISH) -> левый KEY
export const mapValToKey = (val: string, o: { [s: string]: string }) => {
  let entries = Object.entries(o);
  let result = entries.find((el) => el[1] === val);
  if (result) {
    return result[0];
  }
  return entries[0][0];
};

//Преобразование ЛЕВОГО значения (KEY) (ENGLISH) -> ПРАВЫЙ VALUE
export const mapKeyToVal = (key: string, o: { [s: string]: string }) => {
  let entries = Object.entries(o);
  let result = entries.find((el) => el[0] === key);
  if (result) {
    return result[1];
  }
  return entries[0][1];
};

export const mapFormGeoDataToReqLatLng = (geoData: MapGeoData): { geoData: ReqGeoDataCreate } => {
  return {
    geoData: {
      point: {
        lat: geoData.point[0],
        lng: geoData.point[1],
      },
      polygon: geoData.polygon[0].map((el) => {
        return {
          lat: el[0],
          lng: el[1],
        };
      }),
    },
  };
};

export const getFormFieldsByComponent = (component: ResSAComponentAddress | ResSAComponentPlace): {} => {
  return {
    name: component.name,
    type: component.type,
    ...getGeoDataFormFieldsByResGeoData(component.geoData),
  };
};

export const getGeoDataFormFieldsByComponent = (
  component: ResSAComponentAddress | ResSAComponentPlace | null | undefined
): {} => {
  if (!component) return {};
  return getGeoDataFormFieldsByResGeoData(component.geoData);
};

export const getGeoDataFormFieldsByResGeoData = (geoData: ResGeoData | null | undefined): {} => {
  if (!geoData) return {};
  return {
    geoData: {
      point: geoData.point.coordinates,
      polygon: geoData.polygon.coordinates,
    },
  };
};
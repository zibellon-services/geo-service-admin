import 'antd/dist/antd.min.css';
import { configure } from 'mobx';
import ReactDOM from 'react-dom/client';
import { BrowserRouter } from 'react-router-dom';
import { AppStore } from 'stores/app-store';
import { AppScreen } from './App';
import './index.scss';

configure({
  enforceActions: 'never',
});

const root = ReactDOM.createRoot(document.getElementById('root') as HTMLElement);

root.render(
  <BrowserRouter>
    <AppScreen appStore={new AppStore()} />
  </BrowserRouter>
);

FROM node:20.11.1-alpine AS build

WORKDIR /app

RUN apk add --no-cache python3 make g++

ARG REACT_APP_BACK_URL
ENV REACT_APP_BACK_URL=${REACT_APP_BACK_URL}

#Depends
COPY ./package.json ./
COPY ./yarn.lock ./

RUN yarn install --frozen-lockfile

#BUILD
COPY ./public ./public/
COPY ./src ./src/
COPY tsconfig.json ./

RUN yarn build-ci

#################
#Start
#################
FROM node:20.11.1-alpine AS deploy

WORKDIR /app

RUN yarn global add serve

COPY --from=build ./app/build ./build

CMD [ "serve", "-s", "build" ]
